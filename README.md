##Handwriting Recognition

# Instructions

1. *A_Z Handwritten Data.csv* is the dataset file which will take some time to download as it is big
2. Dont run the *OCR.py* file if you have RAM less than 16GB as it takes just 9 Gb to load the dataset
3. *weights.model* is included so that the weights can be directly used to predict stuff without having huge memory of RAM and VRAM to train. 
4. Just create a new python file which uses the above *weights.model* to predict
